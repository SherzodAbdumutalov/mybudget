<?php

namespace App\Http\Requests;

class IncomeUpdateRequest extends MainRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => [
                'required', 'numeric'
            ],
            'description' => [
                'required',
            ],
            'date' => [
                'required', 'date_format:Y-m-d',
            ],
            'budget_id' => [
                'required', 'integer', 'exists:budgets,id'
            ]
        ];
    }
}
