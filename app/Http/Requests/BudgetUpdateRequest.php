<?php

namespace App\Http\Requests;

class BudgetUpdateRequest extends MainRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required', 'string', 'max:255'
            ],
            'start_date' => [
                'required', 'date_format:Y-m-d', 'before_or_equal:end_date'
            ],
            'end_date' => [
                'required', 'date_format:Y-m-d', 'after_or_equal:start_date'
            ],
            'user_id' => [
                'required', 'integer', 'exists:users,id'
            ]
        ];
    }
}
