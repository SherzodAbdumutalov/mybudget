<?php

namespace App\Http\Controllers;

use App\Http\Resources\BudgetResource;
use App\Models\Budget;
use Illuminate\Http\Response;
use App\Http\Requests\BudgetCreateRequest;
use App\Http\Requests\BudgetUpdateRequest;
use App\Interfaces\BudgetServiceInterface;

class BudgetController extends Controller
{
    private $budgetServiceInterface;

    public function __construct(BudgetServiceInterface $budgetServiceInterface)
    {
        $this->budgetServiceInterface = $budgetServiceInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->success(
            BudgetResource::collection(
                $this->budgetServiceInterface->all()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BudgetCreateRequest $request
     * @return Response
     */
    public function store(BudgetCreateRequest $request)
    {
        $budget = $this->budgetServiceInterface->create(
            $request->all()
        );

        return response()->success(new BudgetResource($budget));
    }

    /**
     * Display the specified resource.
     *
     * @param Budget $budget
     * @return Response
     */
    public function show(Budget $budget)
    {
        return response()->success(
            new BudgetResource($this->budgetServiceInterface->getById($budget->id))
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BudgetUpdateRequest $request
     * @param Budget $budget
     * @return Response
     */
    public function update(BudgetUpdateRequest $request, Budget $budget)
    {
        $budget = $this->budgetServiceInterface->update(
            $budget->id,
            $request->all()
        );

        return \response()->success(new BudgetResource($budget));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Budget $budget
     * @return Response
     */
    public function destroy(Budget $budget)
    {
        return \response()->success(
            $this->budgetServiceInterface->delete($budget->id)
        );
    }
}
