<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserFinancialReportResource;
use App\Interfaces\UserServiceInterface;
use App\Models\User;

class UserController extends Controller
{
    private $userServiceInterface;

    public function __construct(UserServiceInterface $userServiceInterface)
    {
        $this->userServiceInterface = $userServiceInterface;
    }

    public function getUserFinancialReport(User $user) {
        return response()->success(
            new UserFinancialReportResource($this->userServiceInterface->getById($user->id))
        );
    }
}
