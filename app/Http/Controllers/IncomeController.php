<?php

namespace App\Http\Controllers;

use App\Models\Income;
use Illuminate\Http\Response;
use App\Http\Resources\IncomeResource;
use App\Http\Requests\IncomeCreateRequest;
use App\Http\Requests\IncomeUpdateRequest;
use App\Interfaces\IncomeServiceInterface;

class IncomeController extends Controller
{
    private $incomeServiceInterface;

    public function __construct(IncomeServiceInterface $incomeServiceInterface)
    {
        $this->incomeServiceInterface = $incomeServiceInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->success(
            IncomeResource::collection(
                $this->incomeServiceInterface->all()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IncomeCreateRequest $request
     * @return Response
     */
    public function store(IncomeCreateRequest $request)
    {
        $income = $this->incomeServiceInterface->create(
            $request->all()
        );

        return response()->success(new IncomeResource($income));
    }

    /**
     * Display the specified resource.
     *
     * @param Income $income
     * @return Response
     */
    public function show(Income $income)
    {
        return response()->success(
            new IncomeResource($this->incomeServiceInterface->getById($income->id))
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param IncomeUpdateRequest $request
     * @param Income $income
     * @return Response
     */
    public function update(IncomeUpdateRequest $request, Income $income)
    {
        $income = $this->incomeServiceInterface->update(
            $income->id,
            $request->all()
        );

        return \response()->success(new IncomeResource($income));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Income $income
     * @return Response
     */
    public function destroy(Income $income)
    {
        return \response()->success(
            $this->incomeServiceInterface->delete($income->id)
        );
    }
}
