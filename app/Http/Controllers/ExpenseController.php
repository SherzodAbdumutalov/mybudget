<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Response;
use App\Http\Resources\ExpenseResource;
use App\Http\Requests\ExpenseCreateRequest;
use App\Http\Requests\ExpenseUpdateRequest;
use App\Interfaces\ExpenseServiceInterface;

class ExpenseController extends Controller
{
    private $expenseServiceInterface;

    public function __construct(ExpenseServiceInterface $expenseServiceInterface)
    {
        $this->expenseServiceInterface = $expenseServiceInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->success(
            ExpenseResource::collection(
                $this->expenseServiceInterface->all()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ExpenseCreateRequest $request
     * @return Response
     */
    public function store(ExpenseCreateRequest $request)
    {
        $expense = $this->expenseServiceInterface->create(
            $request->all()
        );

        return response()->success(new ExpenseResource($expense));
    }

    /**
     * Display the specified resource.
     *
     * @param Expense $expense
     * @return Response
     */
    public function show(Expense $expense)
    {
        return response()->success(
            new ExpenseResource($this->expenseServiceInterface->getById($expense->id))
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ExpenseUpdateRequest $request
     * @param Expense $expense
     * @return Response
     */
    public function update(ExpenseUpdateRequest $request, Expense $expense)
    {
        $expense = $this->expenseServiceInterface->update(
            $expense->id,
            $request->all()
        );

        return \response()->success(new ExpenseResource($expense));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Expense $expense
     * @return Response
     */
    public function destroy(Expense $expense)
    {
        return \response()->success(
            $this->expenseServiceInterface->delete($expense->id)
        );
    }
}
