<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BudgetReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $totalIncome = $this->incomes?->sum('amount');
        $totalExpense = $this->expenses?->sum('amount');

        return [
            'id' => $this->id,
            'title' => $this->title,
            'total_income' => $totalIncome,
            'total_expense' => $totalExpense,
            'remaining' => $totalIncome - $totalExpense
        ];
    }
}
