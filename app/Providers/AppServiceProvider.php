<?php

namespace App\Providers;

use App\Services\UserService;
use App\Services\BudgetService;
use App\Services\IncomeService;
use App\Services\ExpenseService;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\UserServiceInterface;
use Illuminate\Support\Facades\Response;
use App\Interfaces\BudgetServiceInterface;
use App\Interfaces\ExpenseServiceInterface;
use App\Interfaces\IncomeServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ExpenseServiceInterface::class, ExpenseService::class);
        $this->app->bind(BudgetServiceInterface::class, BudgetService::class);
        $this->app->bind(IncomeServiceInterface::class, IncomeService::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($value, $msg = null) {
            return Response::make([
                'code' => 200,
                'data' => $value,
                'message' => $msg
            ]);
        });

        Response::macro('fail', function ($msg, $code, $error = null) {
            return Response::make([
                'code' => $code,
                'data' => $error,
                'message' => $msg
            ]);
        });
    }
}
