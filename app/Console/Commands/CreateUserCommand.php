<?php

namespace App\Console\Commands;

use App\Services\AuthService;
use Illuminate\Console\Command;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates user';
    private $authService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AuthService $authService)
    {
        parent::__construct();

        $this->authService = $authService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->authService->createUser(
            $this->argument('name'),
            $this->argument('email'),
            $this->argument('password')
        );

        $this->info("User created!");

        return 0;
    }
}
