<?php

namespace App\Services;

use App\Models\Income;
use App\Interfaces\IncomeServiceInterface;
use Illuminate\Database\Eloquent\Collection;

class IncomeService implements IncomeServiceInterface
{
    public function all(): Collection
    {
        return Income::query()->get();
    }

    public function getById(int $id): Income
    {
        return Income::query()->findOrFail($id);
    }

    public function create(
        array $attributes
    ): Income
    {
        return Income::query()->create($attributes);
    }

    public function update(
        int $id,
        array $attributes
    ): Income
    {
        $income = Income::query()->findOrFail($id);

        $income->update($attributes);

        return $income->refresh();
    }

    public function delete(int $id): bool
    {
        $income = Income::query()->findOrFail($id);

        $income->delete();

        return true;
    }
}
