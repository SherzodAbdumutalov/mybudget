<?php

namespace App\Services;

use App\Models\Expense;
use App\Interfaces\ExpenseServiceInterface;
use Illuminate\Database\Eloquent\Collection;

class ExpenseService implements ExpenseServiceInterface
{
    public function all(): Collection
    {
        return Expense::query()->get();
    }

    public function getById(int $id): Expense
    {
        return Expense::query()->findOrFail($id);
    }

    public function create(
        array $attributes
    ): Expense
    {
        return Expense::query()->create($attributes);
    }

    public function update(
        int $id,
        array $attributes
    ): Expense
    {
        $expense = Expense::query()->findOrFail($id);

        $expense->update($attributes);

        return $expense->refresh();
    }

    public function delete(int $id): bool
    {
        $expense = Expense::query()->findOrFail($id);

        $expense->delete();

        return true;
    }
}
