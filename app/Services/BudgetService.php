<?php

namespace App\Services;

use App\Models\Budget;
use App\Interfaces\BudgetServiceInterface;
use Illuminate\Database\Eloquent\Collection;

class BudgetService implements BudgetServiceInterface
{
    public function all(): Collection
    {
        return Budget::query()->get();
    }

    public function getById(int $id): Budget
    {
        return Budget::query()->findOrFail($id);
    }

    public function create(
        array $attributes
    ): Budget
    {
        return Budget::query()->create($attributes);
    }

    public function update(
        int $id,
        array $attributes
    ): Budget
    {
        $budget = Budget::query()->findOrFail($id);

        $budget->update($attributes);

        return $budget->refresh();
    }

    public function delete(int $id): bool
    {
        $budget = Budget::query()->findOrFail($id);

        $budget->delete();

        return true;
    }
}
