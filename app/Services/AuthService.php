<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function createUser($name, $email, $password): User
    {
        return User::query()->create([
            'email' => $email,
            'name' => $name,
            'password' => Hash::make($password)
        ]);
    }
}
