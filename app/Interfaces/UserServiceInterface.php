<?php

namespace App\Interfaces;

interface UserServiceInterface
{
    public function getById(int $id);
}
