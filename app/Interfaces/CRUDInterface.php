<?php

namespace App\Interfaces;

interface CRUDInterface
{
    public function all();
    public function getById(int $id);
    public function create(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
