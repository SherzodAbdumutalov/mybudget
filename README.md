# Introduction

Laravel and PHP Budget Management System Test

## Use Cases

It is used to manage budgets.

## requirements

1. php^8.0
2. composer
3. redis
4. pgsql
5. nginx

## Installation

1. get project from gitlab
```json
git clone {url}
```

2. run package manager
```json
composer install
```

3. copy .env.example and fill with database credentials
```json
cp .env.example .env

APP_NAME=MyBudget
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=pgsql
DB_HOST=
DB_PORT=5432
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

```

4. generate key
```json
php artisan key:generate
```

5. run migration
```json
php artisan migrate
```

6. create user
```json
php artisan user:create name email password
```
7. create jwt secret
```json
php artisan jwt:secret
```

## Models

#### user
| column | type 
| :--- | :--- |
| `name` | `string` |
| `email` | `string` |
| `email_verified_at` | `timestamp` |
| `password` | `string` |
| `remember_token` | `string` |
| `created_at` | `timestamp` |
| `updated_at` | `timestamp` |

#### budget
| column | type
| :--- | :--- |
| `title` | `string` |
| `start_date` | `date` |
| `end_date` | `date` |
| `user_id` | `big integer` |
| `created_at` | `timestamp` |
| `updated_at` | `timestamp` |

#### expense
| column | type
| :--- | :--- |
| `amount` | `double` |
| `description` | `text` |
| `category` | `string` |
| `date` | `date` |
| `budget_id` | `big integer` |
| `created_at` | `timestamp` |
| `updated_at` | `timestamp` |

#### income
| column | type
| :--- | :--- |
| `amount` | `double` |
| `description` | `text` |
| `date` | `date` |
| `budget_id` | `big integer` |
| `created_at` | `timestamp` |
| `updated_at` | `timestamp` |

## API docs

#### Response codes

| Code | Description
| :--- | :--- |
| `200` | Success Response |
| `500` | Internal error Response |
| `404` | Not Found |
| `402` | Validation error |
| `401` | not Authorized |

#### Responses structure
```json
{
  "code"    : integer,
  "data"    : string,
  "message" : string
}
```

The `message` attribute contains a message commonly used to indicate errors or, in the case of deleting a resource, success that the resource was properly deleted.

The `code` attribute describes if the request was successful or not.

The `data` attribute contains any other metadata associated with the response. This will be an escaped string containing JSON data.

## Login

Authentication is realized via JWT.

```http
POST /api/auth/login
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `email` | `string` | **Required**. Your Email |
| `password` | `string` | **Required**. Your password |

#### response
```json
{
    "code": 200,
    "data": {
        "access_token": "token",
        "token_type": "Bearer"
    },
    "message": null
}
```

## get authed user

```http
GET /api/auth/user
```

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "name": "string",
        "email": "string",
        "email_verified_at": "timestamp",
        "created_at": "timestamp",
        "updated_at": "timestamp"
    },
    "message": null
}
```


## Create user

```http
POST /api/auth/register
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `name` | `string` | **Required**. Your Name |
| `email` | `string` | **Required**. Your Email |
| `password` | `string` | **Required**. Your password |

#### response
```json
{
    "code": 200,
    "data": {
        "access_token": "token",
        "token_type": "Bearer",
        "expires_in": 3600
    },
    "message": null
}
```

## Get budgets

```http
GET /api/budgets
```

#### response
```json
{
    "code": 200,
    "data": [
        ...,
        {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        },
        ...
    ],
    "message": null
}
```


## Get budget

```http
GET /api/budgets/{id}
```

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "title": "string",
        "start_date": "date",
        "end_date": "date",
        "user": {
            "id": "integer",
            "name": "string",
            "email": "string"
        }
    },
    "message": null
}
```

## Create budget

```http
POST /api/budgets
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `title` | `string` | **Required**. Your Name |
| `start_date` | `string` | **Required**, **Date** start date |
| `end_date` | `string` | **Required**, **Date** end date |
| `user_id` | `string` | **Required**, **Integer** |

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "title": "string",
        "start_date": "date",
        "end_date": "date",
        "user": {
            "id": "integer",
            "name": "string",
            "email": "string"
        }
    },
    "message": null
}
```


## Update budget

```http
PUT /api/budgets/{id}
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `title` | `string` | **Required**. Your Name |
| `start_date` | `string` | **Required**, **Date** start date |
| `end_date` | `string` | **Required**, **Date** end date |
| `user_id` | `string` | **Required**, **Integer** |

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "title": "string",
        "start_date": "date",
        "end_date": "date",
        "user": {
            "id": "integer",
            "name": "string",
            "email": "string"
        }
    },
    "message": null
}
```

## Delete budget

```http
DELETE /api/budgets/{id}
```

#### response
```json
{
    "code": 200,
    "data": "bool",
    "message": null
}
```

## Get expenses

```http
GET /api/expenses
```

#### response
```json
{
    "code": 200,
    "data": [
        ...,
        {
            "id": "integer",
            "amount": "numeric",
            "description": "string",
            "category": "string",
            "date": "date",
            "budget": {
                "id": "integer",
                "title": "string",
                "start_date": "date",
                "end_date": "date",
                "user": {
                    "id": "integer",
                    "name": "string",
                    "email": "string"
                }
            }
        },
        ...
    ],
    "message": null
}
```


## Get expense

```http
GET /api/expenses/{id}
```

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "amount": "numeric",
        "description": "string",
        "category": "string",
        "date": "date",
        "budget": {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        }
    },
    "message": null
}
```

## Create expense

```http
POST /api/expenses
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `amount` | `integer` | **Required**, **numeric** amount |
| `description` | `string` | **Required**, **string** description |
| `category` | `string` | **Required**, **String** category |
| `date` | `string` | **Required**, **Date** date |
| `budget_id` | `string` | **Required**, **Integer** Budget |

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "amount": "numeric",
        "description": "string",
        "category": "string",
        "date": "date",
        "budget": {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        }
    },
    "message": null
}
```


## Update expense

```http
PUT /api/expense/{id}
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `amount` | `integer` | **Required**, **numeric** amount |
| `description` | `string` | **Required**, **string** description |
| `category` | `string` | **Required**, **String** category |
| `date` | `string` | **Required**, **Date** date |
| `budget_id` | `string` | **Required**, **Integer** Budget |

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "amount": "numeric",
        "description": "string",
        "category": "string",
        "date": "date",
        "budget": {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        }
    },
    "message": null
}
```

## Delete expense

```http
DELETE /api/expenses/{id}
```

#### response
```json
{
    "code": 200,
    "data": "bool",
    "message": null
}
```

## Get incomes

```http
GET /api/incomes
```

#### response
```json
{
    "code": 200,
    "data": [
        ...,
        {
            "id": "integer",
            "amount": "numeric",
            "description": "string",
            "date": "date",
            "budget": {
                "id": "integer",
                "title": "string",
                "start_date": "date",
                "end_date": "date",
                "user": {
                    "id": "integer",
                    "name": "string",
                    "email": "string"
                }
            }
        },
        ...
    ],
    "message": null
}
```


## Get income

```http
GET /api/incomes/{id}
```

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "amount": "numeric",
        "description": "string",
        "date": "date",
        "budget": {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        }
    },
    "message": null
}
```

## Create income

```http
POST /api/incomes
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `amount` | `integer` | **Required**, **numeric** amount |
| `description` | `string` | **Required**, **string** description |
| `date` | `string` | **Required**, **Date** date |
| `budget_id` | `string` | **Required**, **Integer** Budget |

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "amount": "numeric",
        "description": "string",
        "date": "date",
        "budget": {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        }
    },
    "message": null
}
```


## Update income

```http
PUT /api/incomes/{id}
```

#### request payloads

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `amount` | `integer` | **Required**, **numeric** amount |
| `description` | `string` | **Required**, **string** description |
| `date` | `string` | **Required**, **Date** date |
| `budget_id` | `string` | **Required**, **Integer** Budget |

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "amount": "numeric",
        "description": "string",
        "date": "date",
        "budget": {
            "id": "integer",
            "title": "string",
            "start_date": "date",
            "end_date": "date",
            "user": {
                "id": "integer",
                "name": "string",
                "email": "string"
            }
        }
    },
    "message": null
}
```

## Delete income

```http
DELETE /api/incomes/{id}
```

#### response
```json
{
    "code": 200,
    "data": "bool",
    "message": null
}
```

## Get user financial status

```http
GET /api/users/{id}/financial-report
```

#### response
```json
{
    "code": 200,
    "data": {
        "id": "integer",
        "name": "string",
        "email": "string",
        "budgets": [
            ...,
            {
                "id": "integer",
                "title": "string",
                "total_income": "numeric",
                "total_expense": "numeric",
                "remaining": "numeric",
            },
            ...
        ]
    },
    "message": null
}
```

## Run Tests

```json
php artisan test
```
