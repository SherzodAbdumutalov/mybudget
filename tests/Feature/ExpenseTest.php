<?php

namespace Tests\Feature;

use App\Models\Budget;
use App\Models\Expense;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExpenseTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_get_all_expenses()
    {
        $user = User::factory()->create();

        Budget::factory()->create();
        Expense::factory()->count(3)->create();

        $response = $this->actingAs($user)->get('/api/expenses');
        $response
            ->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertJsonStructure([
                'code',
                'data' => [
                    '*' => $this->getExpenseResponseStructure(),
                ],
                'message'
            ])->assertJson([
                'code' => 200
            ]);
    }

    public function test_get_expense_by_id()
    {
        $user = User::factory()->create();

        $budget = Budget::factory()->create();
        $expense = Expense::factory()->create(['budget_id' => $budget->id]);

        $response = $this->actingAs($user)->get('/api/expenses/' . $expense->id);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => [
                    'id' => $expense->id,
                    'amount' => $expense->amount,
                    'date' => $expense->date,
                    'category' => $expense->category,
                    'description' => $expense->description,
                ],
                'message' => null
            ]);
    }

    public function test_create_expense()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create();

        $response = $this->actingAs($user)->post('/api/expenses', [
            'amount' => 111,
            'date' => '2022-01-01',
            'category' => 'my_category',
            'description' => 'description',
            'budget_id' => $budget->id
        ]);

        $response
            ->assertStatus(200)
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => $this->getExpenseResponseStructure(),
                'message'
            ])
            ->assertJson([
                'code' => 200
            ]);

        $this->assertDatabaseHas('expenses', [
            'amount' => 111,
            'date' => '2022-01-01',
            'category' => 'my_category',
            'description' => 'description',
            'budget_id' => $budget->id
        ]);
    }

    public function test_create_expense_without_required_fields()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/api/expenses', []);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The amount field is required.",
                    "The description field is required.",
                    "The date field is required.",
                    "The category field is required.",
                    "The budget id field is required."
                ],
                'message' => 'Data is invalid.'
            ]);
    }

    public function test_create_expense_with_amount_as_string()
    {
        $user = User::factory()->create();

        $expense = $this->makeExpense();

        $response = $this->actingAs($user)->post('/api/expenses', [
            'amount' => 'aaaaa',
            'budget_id' => $expense['budget_id'],
            'date' => $expense['date'],
            'description' => $expense['description'],
            'category' => $expense['category']
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The amount must be a number."
                ],
                'message' => 'Data is invalid.'
            ]);
    }

    public function test_update_expense()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create(['user_id' => $user->id]);
        $expense = Expense::factory()->create(['budget_id' => $budget->id]);

        $response = $this->actingAs($user)->put('/api/expenses/' . $expense->id, [
            'amount' => 100,
            'date' => '2022-11-01',
            'description' => 'description updated',
            'category' => 'updated_category',
            'budget_id' => $budget->id
        ]);

        $response->assertStatus(200)
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => $this->getExpenseResponseStructure(),
                'message'
            ]);

        $this->assertDatabaseHas('expenses', [
            'amount' => 100,
            'date' => '2022-11-01',
            'description' => 'description updated',
            'category' => 'updated_category',
            'budget_id' => $budget->id
        ]);
    }

    public function test_delete_expense()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create(['user_id' => $user->id]);
        $expense = Expense::factory()->create(['budget_id' => $budget->id]);

        $response = $this->actingAs($user)->delete('/api/expenses/' . $expense->id);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => true,
                'message' => null
            ]);

        $this->assertDatabaseMissing('expenses', [
            'id' => $expense->id,
        ]);
    }

    public function getExpenseResponseStructure() {
        return [
            "id",
            "amount",
            "description",
            "date",
            "category",
            "budget" => [
                "id",
                "title",
                "start_date",
                "end_date",
                "user" => [
                    "id",
                    "name",
                    "email"
                ]
            ]
        ];
    }

    public function makeExpense(): array
    {
        $expense = Expense::factory()->make();

        return [
            'amount' => $expense->amount,
            'date' => $expense->date,
            'description' => $expense->description,
            'category' => $expense->category,
            'budget_id' => $expense->budget_id,
        ];
    }
}
