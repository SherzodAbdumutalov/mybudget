<?php

namespace Tests\Feature;

use App\Models\Budget;
use App\Models\Income;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IncomeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_get_all_incomes()
    {
        $user = User::factory()->create();

        Budget::factory()->create();
        Income::factory()->count(3)->create();

        $response = $this->actingAs($user)->get('/api/incomes');
        $response
            ->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertJsonStructure([
                'code',
                'data' => [
                    '*' => $this->getIncomeResponseStructure(),
                ],
                'message'
            ])->assertJson([
                'code' => 200
            ]);
    }

    public function test_get_income_by_id()
    {
        $user = User::factory()->create();

        $budget = Budget::factory()->create();
        $income = Income::factory()->create(['budget_id' => $budget->id]);

        $response = $this->actingAs($user)->get('/api/incomes/' . $income->id);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => [
                    'id' => $income->id,
                    'amount' => $income->amount,
                    'date' => $income->date,
                    'description' => $income->description,
                ],
                'message' => null
            ]);
    }

    public function test_create_income()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create();

        $response = $this->actingAs($user)->post('/api/incomes', [
            'amount' => 111,
            'date' => '2022-01-01',
            'description' => 'description',
            'budget_id' => $budget->id
        ]);

        $response
            ->assertStatus(200)
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => $this->getIncomeResponseStructure(),
                'message'
            ])
            ->assertJson([
                'code' => 200
            ]);

        $this->assertDatabaseHas('incomes', [
            'amount' => 111,
            'date' => '2022-01-01',
            'description' => 'description',
            'budget_id' => $budget->id
        ]);
    }

    public function test_create_income_without_required_fields()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/api/incomes', []);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The amount field is required.",
                    "The description field is required.",
                    "The date field is required.",
                    "The budget id field is required."
                ],
                'message' => 'Data is invalid.'
            ]);
    }

    public function test_create_income_with_amount_as_string()
    {
        $user = User::factory()->create();

        $income = $this->makeIncome();

        $response = $this->actingAs($user)->post('/api/incomes', [
            'amount' => 'aaaaa',
            'budget_id' => $income['budget_id'],
            'date' => $income['date'],
            'description' => $income['description'],
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The amount must be a number."
                ],
                'message' => 'Data is invalid.'
            ]);
    }

    public function test_update_income()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create(['user_id' => $user->id]);
        $income = Income::factory()->create(['budget_id' => $budget->id]);

        $response = $this->actingAs($user)->put('/api/incomes/' . $income->id, [
            'amount' => 100,
            'date' => '2022-11-01',
            'description' => 'description updated',
            'budget_id' => $budget->id
        ]);

        $response->assertStatus(200)
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => $this->getIncomeResponseStructure(),
                'message'
            ]);

        $this->assertDatabaseHas('incomes', [
            'amount' => 100,
            'date' => '2022-11-01',
            'description' => 'description updated',
            'budget_id' => $budget->id
        ]);
    }

    public function test_delete_income()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create(['user_id' => $user->id]);
        $income = Income::factory()->create(['budget_id' => $budget->id]);

        $response = $this->actingAs($user)->delete('/api/incomes/' . $income->id);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => true,
                'message' => null
            ]);

        $this->assertDatabaseMissing('incomes', [
            'id' => $income->id,
        ]);
    }

    public function getIncomeResponseStructure() {
        return [
            "id",
            "amount",
            "description",
            "date",
            "budget" => [
                "id",
                "title",
                "start_date",
                "end_date",
                "user" => [
                    "id",
                    "name",
                    "email"
                ]
            ]
        ];
    }

    public function makeIncome(): array
    {
        $income = Income::factory()->make();

        return [
            'amount' => $income->amount,
            'date' => $income->date,
            'description' => $income->description,
            'category' => $income->category,
            'budget_id' => $income->budget_id,
        ];
    }
}
