<?php

namespace Tests\Feature;

use App\Models\Budget;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BudgetTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_get_all_budgets()
    {
        $user = User::factory()->create();

        Budget::factory()->count(3)->create();

        $response = $this->actingAs($user)->get('/api/budgets');

        $response->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertJsonStructure([
                'code',
                'data' => [
                    '*' => $this->getBudgetResponseStructure(),
                ],
                'message'
            ])->assertJson([
                'code' => 200
            ]);
    }

    public function test_get_budget_by_id()
    {
        $user = User::factory()->create();

        $budget = Budget::factory()->create();

        $response = $this->actingAs($user)->get('/api/budgets/' . $budget->id);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => [
                    'id' => $budget->id,
                    'title' => $budget->title,
                    'start_date' => $budget->start_date,
                    'end_date' => $budget->end_date,
                ],
                'message' => null
            ]);
    }

    public function test_not_found_budget_by_id()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/api/budgets/999999999');

        $response->assertStatus(200)
            ->assertJson([
                'code' => 404,
                'data' => null,
                'message' => "Model not found"
            ]);
    }

    public function test_create_budget()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/api/budgets', [
            'title' => 'Test Budget',
            'start_date' => '2022-01-01',
            'end_date' => '2022-01-10',
            'user_id' => $user->id
        ]);

        $response
            ->assertStatus(200)
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => $this->getBudgetResponseStructure(),
                'message'
            ]);

        $this->assertDatabaseHas('budgets', [
            'title' => 'Test Budget',
            'start_date' => '2022-01-01',
            'end_date' => '2022-01-10',
            'user_id' => $user->id
        ]);
    }

    public function test_create_budget_without_required_fields()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/api/budgets', []);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The title field is required.",
                    "The start date field is required.",
                    "The end date field is required.",
                    "The user id field is required."
                ],
                'message' => 'Data is invalid.'
            ]);
    }

    public function test_create_budget_with_incorrect_start_and_end_date_period()
    {
        $user = User::factory()->create();

        $budget = $this->makeBudget();

        $response = $this->actingAs($user)->post('/api/budgets', [
            'title' => $budget['title'],
            'user_id' => $budget['user_id'],
            'start_date' => '2022-01-10',
            'end_date' => '2022-01-01'
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The start date must be a date before or equal to end date.",
                    "The end date must be a date after or equal to start date."
                ],
                'message' => 'Data is invalid.'
            ]);
    }

    public function test_update_budget()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create(['user_id' => $user->id]);

        $response = $this->actingAs($user)->put('/api/budgets/' . $budget->id, [
            'title' => 'Updated Budget',
            'start_date' => '2022-11-01',
            'end_date' => '2022-11-10',
            'user_id' => $user->id
        ]);

        $response->assertStatus(200)
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => $this->getBudgetResponseStructure(),
                'message'
            ]);

        $this->assertDatabaseHas('budgets', [
            'title' => 'Updated Budget',
            'start_date' => '2022-11-01',
            'end_date' => '2022-11-10',
            'user_id' => $user->id
        ]);
    }

    public function test_delete_budget()
    {
        $user = User::factory()->create();
        $budget = Budget::factory()->create(['user_id' => $user->id]);

        $response = $this->actingAs($user)->delete('/api/budgets/' . $budget->id);

        $response->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => true,
                'message' => null
            ]);

        $this->assertDatabaseMissing('budgets', [
            'id' => $budget->id,
        ]);
    }

    public function getBudgetResponseStructure() {
        return [
            "id",
            "title",
            "start_date",
            "end_date",
            "user" => [
                "id",
                "name",
                "email"
            ]
        ];
    }

    public function makeBudget(): array
    {
        $budget = Budget::factory()->make();

        return [
            'title' => $budget->title,
            'user_id' => $budget->user_id,
            'start_date' => Carbon::now()->format('Y-m-d'),
            'end_date' => Carbon::tomorrow()->format('Y-m-d')
        ];
    }
}
