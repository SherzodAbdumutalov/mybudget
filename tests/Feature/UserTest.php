<?php

namespace Tests\Feature;

use App\Models\Budget;
use App\Models\Expense;
use App\Models\Income;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_login_with_required_fields(): void
    {
        User::factory()->create([
            'email' => 'johndoe@example.com',
            'password' => bcrypt('password'),
        ]);

        $response = $this->post('/api/auth/login', [
            'email' => 'johndoe@example.com',
            'password' => 'password',
        ]);

        $response->assertSuccessful()
            ->assertValid()
            ->assertJsonStructure([
                'code',
                'data' => ['access_token', 'token_type'],
                'message'
            ])->assertJson([
                'code' => 200
            ]);
    }

    public function test_user_can_login_without_required_fields(): void
    {
        $response = $this->post('/api/auth/login', []);

        $response->assertSuccessful()
            ->assertJson([
                'code' => 422,
                'data' => [
                    "The email field is required.",
                    "The password field is required."
                ],
                'message' => "Data is invalid."
            ]);
    }

    public function test_user_cannot_login_with_required_fields(): void
    {
        $response = $this->post('/api/auth/login', [
            'email' => 'nouserwiththisemailorincorrectpassword@email.com',
            'password' => 'nouserwiththisemailorincorrectpassword@email.com'
        ]);

        $response->assertSuccessful()
            ->assertValid()
            ->assertJson([
                'code' => 401,
                'data' => null,
                'message' => "Email & Password does not match with our record."
            ]);
    }

    public function test_authed_user(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->get('/api/auth/me');

        $response->assertSuccessful()
            ->assertJsonStructure([
                'code',
                'data' => ['name', 'email'],
                'message'
            ])->assertJson([
                'code' => 200
            ]);
    }

    public function test_user_report(): void
    {
        $user = User::factory()->create();
        Budget::factory()->count(3)->create(['user_id' => $user->id]);
        Income::factory()->count(10)->create();
        Expense::factory()->count(10)->create();
        $response = $this->actingAs($user)->get("/api/users/{$user->id}/financial-report");
        $response->assertSuccessful()
            ->assertJsonStructure([
                'code',
                'data' => [
                    'id', 'name', 'email',
                    'budgets' => [
                        '*' => [
                            'id', 'title', 'total_income', 'total_expense', 'remaining'
                        ]
                    ]
                ],
                'message'
            ])->assertJson(['code' => 200]);
    }

    public function makeUser(): array
    {
        $user = User::factory()->make();

        return [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '123'
        ];
    }
}
