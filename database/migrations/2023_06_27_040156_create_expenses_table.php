<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();
            $table->float('amount');
            $table->text('description');
            $table->string('category');
            $table->date('date');
            $table->bigInteger('budget_id');
            $table->foreign('budget_id')->on('budgets')->references('id')->onDelete('CASCADE');
            $table->timestamps();

            $table->index(['budget_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
