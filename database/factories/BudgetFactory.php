<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BudgetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $startDate = $this->faker->dateTimeBetween('next Monday', 'next Monday +7 days');
        $endDate = $this->faker->dateTimeBetween($startDate, $startDate->format('Y-m-d').' +2 days');
        $users = User::query()->get()->keyBy('id')->keys()->toArray();

        return [
            'title' => $this->faker->title,
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d'),
            'user_id' => $this->faker->randomElement($users)
        ];
    }
}
