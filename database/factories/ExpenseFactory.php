<?php

namespace Database\Factories;

use App\Models\Budget;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExpenseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $budgets = Budget::query()->get()->keyBy('id')->keys()->toArray();

        return [
            'amount' => $this->faker->numberBetween(0, 100),
            'date' => $this->faker->date('Y-m-d'),
            'description' => $this->faker->text,
            'category' => $this->faker->word,
            'budget_id' => $this->faker->randomElement($budgets)
        ];
    }
}
