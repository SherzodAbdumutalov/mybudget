<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::get('/me', [\App\Http\Controllers\AuthController::class, 'me'])->middleware('auth:api');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('budgets', \App\Http\Controllers\BudgetController::class);
    Route::apiResource('expenses', \App\Http\Controllers\ExpenseController::class);
    Route::apiResource('incomes', \App\Http\Controllers\IncomeController::class);
    Route::get('users/{user}/financial-report', [\App\Http\Controllers\UserController::class, 'getUserFinancialReport']);
});
